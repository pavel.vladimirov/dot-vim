" Pathogen is used to manage vim plugins
execute pathogen#infect()

filetype plugin indent on

set mouse=a

set ts=2 sts=2 sw=2 noexpandtab
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix

" add ctrlp plugin
set runtimepath^=~/.vim/bundle/ctrlp.vim
" path to colorscheme
set runtimepath+=~/.vim/bundle/colors/
" add lightline.vim
"set runtimepath+=~/.vim/bundle/lightline.vim
" add powerline to vim
set rtp+=$HOME/.local/lib/python2.7/site-packages/powerline/bindings/vim/

syntax enable
colorscheme darcula

" Always show statusline
set laststatus=2

" Use 256 colours (Use this setting only if your terminal supports 256 colours)
set t_Co=256

" define Leader key = <Space>
let mapleader=" "
" toggle line numbers by pressing <F2>
nnoremap <F2> : set nu!<CR>
" execute current file
nnoremap <F9> :!%:p<Enter>
nnoremap <leader>r :!%:p<Enter>
" search visually selected text
vnoremap // y/<C-R>"<CR>

highlight def link InpKey  Identifier 
highlight def link InpValue Statement

" Enable folding
set foldmethod=indent
set foldlevel=99
" Enable folding with the spacebar
nnoremap <space> za
" set nocompatible
" set mouse=a
" set cursorline
"
"
" options for YCM = YouCompleteMe
set encoding=utf-8

