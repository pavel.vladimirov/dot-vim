Installation:

	git clone https://gitlab.com/pavel.vladimirov/dot-vim.git ~/.vim

Create symlinks:

    ln -s ~/.vim/vimrc ~/.vimrc
    ln -s ~/.vim/gvimrc ~/.gvimrc

Switch to the `~/.vim` directory, and fetch submodules:

    cd ~/.vim
    git submodule init
    git submodule update

To install YouCompleteMe (YCM) more efforts are required:

	cd ~/.vim/bundle/YouCompleteMe
	git submodule update --init --recursive
	# if not yet installed cmake
	# check with <brew list cmake>
	brew install Cmake # (example for macOs)
	# this line is to enable linking of dynamic libraries
	export PYTHON_CONFIGURE_OPTS="--enable-framework"
	# final installation of YCM
	./install.py --clang-completer --system-libclang

If YCM is not working reinstall vim

	sudo apt install vim # (example for ubuntu)

Check vim version with

	vim --version

It is often the case that the old version is on the PATH. Usual places for vim installation are:

	/usr/bin/
	/usr/local/bin

Rename the old version of vim and make sure that new version of on the PATH and can be found:

	which vim
	$(which vim) --version

If bash is still looking for vim at the old location use

	rehash vim

Alias to the new version can be used as well

	alias vim=/new/vim/path/vim
